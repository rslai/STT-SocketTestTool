package org.rslai.socket.testtool.isocket;

/**
 * 保存接收到的数据（抽象类）
 * @author rslai
 *
 */
public abstract class ReceiveData {
	protected int bodyLength; // 接收的数据的体信息的长度
	protected StringBuffer body; // 接收的数据的体信息

	abstract public String toString();

	/**
	 * 设置头信息
	 * @param head
	 * @throws Exception 
	 */
	abstract public void setHead(Object head);

	/**
	 * 返回头信息
	 * @return 返回接收到的头信息
	 */
	abstract public String getHead();

	/**
	 * 返回体信息长度
	 * @return 体信息长度
	 */
	public int getBodyLength() {
		return bodyLength;
	}

	/**
	 * 设置体信息
	 * @param body 体信息
	 */
	public void setBody(StringBuffer body) {
		this.body = body;
	}

	/**
	 * 返回体信息
	 * @return 返回接收到的体信息
	 */
	public String getBody() {
		return body.toString();
	}

	/**
	 * 根据左右边界（lb,rb）从head部分中截取相应内容
	 * @param lb 左边界
	 * @param rb 右边界
	 * @return 返回找到的字符串 或 ""
	 */
	abstract public String getSpecifyStrHead(String lb, String rb);

	/**
	 * 根据左右边界（lb,rb）从head部分中截取相应内容
	 * @param lb 左边界
	 * @param lbOrder　左边界出现的次数
	 * @param rb 右边界
	 * @param rbOrder 从左边界之后，右边界出现的次数
	 * @return 返回找到的字符串 或 ""
	 */
	abstract public String getSpecifyStrHead(String lb, int lbOrder, String rb, int rbOrder);

	/**
	 * 根据左右边界（lb,rb）从head部分中截取相应内容
	 * @param lb 左边界
	 * @param rb 右边界
	 * @param Order 出现的次数
	 * @return 返回找到的字符串 或 ""
	 */
	abstract public String getSpecifyStrHead(String lb, String rb, int Order);

	/**
	 * 根据左右边界（lb,rb）从body部分中截取相应内容
	 * @param lb 左边界
	 * @param rb 右边界
	 * @return 返回找到的字符串 或 ""
	 */
	public String getSpecifyStrBody(String lb, String rb) {
		return org.rslai.socket.testtool.util.StrHandle.substring(body.toString(), lb, rb);
	}

	/**
	 * 根据左右边界（lb,rb）从body部分中截取相应内容
	 * @param lb 左边界
	 * @param lbOrder　左边界出现的次数
	 * @param rb 右边界
	 * @param rbOrder 从左边界之后，右边界出现的次数
	 * @return 返回找到的字符串 或 ""
	 */
	public String getSpecifyStrBody(String lb, int lbOrder, String rb, int rbOrder) {
		return org.rslai.socket.testtool.util.StrHandle.substring(body.toString(), lb, lbOrder, rb, rbOrder);
	}

	/**
	 * 根据左右边界（lb,rb）从body部分中截取相应内容
	 * @param lb 左边界
	 * @param rb 右边界
	 * @param rbOrder 出现的次数
	 * @return 返回找到的字符串 或 ""
	 */
	public String getSpecifyStrBody(String lb, String rb, int rbOrder) {
		return org.rslai.socket.testtool.util.StrHandle.substring(body.toString(), lb, rb, rbOrder);
	}

}