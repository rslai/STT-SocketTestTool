package org.rslai.socket.testtool.isocket;

/**
 * 返回ISocket的工厂类
 * @author rslai
 *
 */
public class ISocketFactory {
	private String server;
	private int port;
	private String charset;

	/**
	 * 构造函数
	 * @param server 服务器IP或域名
	 * @param port 服务端口
	 * @param charset 传送信息的字符集
	 */
	public ISocketFactory(String server, int port, String charset) {
		this.server = server;
		this.port = port;
		this.charset = charset;
	}

	/**
	 * 返回相应的ISocket的实现类
	 * @param className 想得到的实现类名称
	 * @return 返回的实现类
	 * @throws Exception 
	 */
	public ISocket getISocket(String className) throws Exception {
		ISocket tmpSocket = null;

		if (className.equals(ISocketP2P.class.getName())) {
			// 返回P2P的实现类
			tmpSocket = new ISocketP2P(this.server, this.port, this.charset);
		} else if (className.equals(ISocketDotNet.class.getName())) {
			// 返回DotNet的实现类
			tmpSocket = new ISocketDotNet(this.server, this.port, this.charset);
		} else if (className.equals(ISocketIM.class.getName())) {
			// 返回IM的实现类
			tmpSocket = new ISocketIM(this.server, this.port, this.charset);
		} else if (className.equals(ISocketFinancier5.class.getName())) {
			// 返回财讯服务器5.0的实现
			tmpSocket = new ISocketFinancier5(this.server, this.port, this.charset);
		} else {
			throw new Exception("无法找到实现类：" + className + " 请确认此实现类名称。");
		}

		return tmpSocket;
	}

}
