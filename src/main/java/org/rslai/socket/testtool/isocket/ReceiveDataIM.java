package org.rslai.socket.testtool.isocket;

/**
 * IM 保存接收到的数据
 * @author rslai
 *
 */
public class ReceiveDataIM extends ReceiveData {
	private StringBuffer head; // 收的数据的头信息

	public String toString() {
		StringBuffer str = new StringBuffer();

		str.append(head);
		str.append(body);

		return str.toString();
	}

	/**
	 * 设置头信息
	 * @param head
	 * @throws Exception 
	 */
	public void setHead(Object head) {
			this.head = (StringBuffer)head;

		// 设置体长度
		this.bodyLength = Integer.parseInt(this.getSpecifyStrHead("Content-Length: ", "\r\n"));
	}

	/**
	 * 返回头信息
	 * @return 返回接收到的头信息
	 */
	public String getHead() {
		return head.toString();
	}

	/**
	 * 根据左右边界（lb,rb）从head部分中截取相应内容
	 * @param lb 左边界
	 * @param rb 右边界
	 * @return 返回找到的字符串 或 ""
	 */
	public String getSpecifyStrHead(String lb, String rb) {
		return org.rslai.socket.testtool.util.StrHandle.substring(head.toString() , lb, rb);
	}

	/**
	 * 根据左右边界（lb,rb）从head部分中截取相应内容
	 * @param lb 左边界
	 * @param lbOrder　左边界出现的次数
	 * @param rb 右边界
	 * @param rbOrder 从左边界之后，右边界出现的次数
	 * @return 返回找到的字符串 或 ""
	 */
	public String getSpecifyStrHead(String lb, int lbOrder, String rb, int rbOrder) {
		return org.rslai.socket.testtool.util.StrHandle.substring(head.toString(), lb, lbOrder, rb, rbOrder);
	}

	/**
	 * 根据左右边界（lb,rb）从head部分中截取相应内容
	 * @param lb 左边界
	 * @param rb 右边界
	 * @param Order 出现的次数
	 * @return 返回找到的字符串 或 ""
	 */
	public String getSpecifyStrHead(String lb, String rb, int Order) {
		return org.rslai.socket.testtool.util.StrHandle.substring(head.toString(), lb, rb, Order);
	}

}