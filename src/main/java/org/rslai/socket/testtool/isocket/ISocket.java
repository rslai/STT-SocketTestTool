package org.rslai.socket.testtool.isocket;

import org.apache.log4j.*;
import java.io.*;
import java.net.*;

/**
 * 创建连接并且处理接收、发送数据（抽象类）
 * @author rslai
 *
 */
public abstract class ISocket{
	static Logger logger = Logger.getLogger(ISocket.class);

	protected static int bufSize = 4096; // 读取缓冲区大小

	public static String ASCII = "US-ASCII"; // US-ASCII 7 位 ASCII 字符，也叫作 ISO646-US、Unicode 字符集的基本拉丁块 
	public static String ISO8859_1 = "ISO-8859-1"; // ISO-8859-1   ISO 拉丁字母表 No.1，也叫作 ISO-LATIN-1 
	public static String UTF8 = "UTF-8"; // UTF-8 8 位 UCS 转换格式 
	public static String UTF16BE = "UTF-16BE"; // UTF-16BE 16 位 UCS 转换格式，Big Endian（最低地址存放高位字节）字节顺序 
	public static String UTF16LE = "UTF-16LE"; // UTF-16LE 16 位 UCS 转换格式，Little-endian（最高地址存放低位字节）字节顺序 
	public static String UTF16 = "UTF-16"; // UTF-16 16 位 UCS 转换格式，字节顺序由可选的字节顺序标记来标识 

	protected Socket clientSocket;

	protected DataOutputStream writeBound; // 得到输出流
	protected String charset; // 发送和接收字符的编码方式

	/**
	 * 根据输入的参数建立一个Socket连接
	 * @param charset 传送信息的字符集
	 * @throws IOException
	 */
	public ISocket(String charset) throws IOException{
		this.charset = charset;
	}

	/**
	 * 关闭打开 Socket
	 */
	abstract public void close() throws Exception;

	/**
	 * 向服务器发请求，按字符发送
	 * @param content 发送的字符串
	 * @throws Exception
	 */
	public synchronized void send(String content) throws Exception{
		logger.debug("send : \r\n" + content);

		// 忘记了为什么要这么写到调试时有问题再改
		//writeBound.write(content.getBytes(), 0, content.length());
		writeBound.writeBytes(content);
		writeBound.flush();
	}

	/**
	 * 向服务器发请求，按byte数组发送
	 * @param content 发送byte数组
	 * @throws Exception
	 */
	public synchronized void send(byte[] content) throws Exception{
		logger.debug("send : \r\n" + new String(content, charset));

		writeBound.write(content); 
		writeBound.flush();
	}

	/**
	 * 从服务器接收信息
	 * @return 返回接收到的数据
	 * @throws Exception
	 */
	abstract public ReceiveData receive() throws Exception;

}
