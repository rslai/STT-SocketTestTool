package org.rslai.socket.testtool.isocket;

/**
 * P2P 保存接收到的数据
 * @author rslai
 *
 */
public class ReceiveDataP2P extends ReceiveData {
	private byte[] head; // 接收的数据的头信息

	public String toString() {
		StringBuffer str = new StringBuffer();

		str.append(new String(head));
		str.append(body);

		return str.toString();
	}

	/**
	 * 设置头信息
	 * @param head
	 */
	public void setHead(Object head) {
		this.head = (byte[])head;

		// 这里读取了4个字节
		this.bodyLength = (((int)this.head[1]&0xFF<<32)) + (((int)this.head[2]&0xFF)<<16) + (((int)this.head[3]&0xFF)<<8) + ((int)this.head[4]&0xFF);
	}

	/**
	 * 返回头信息
	 * @return 返回接收到的头信息
	 */
	public String getHead() {
		return new String(head);
	}

	/**
	 * 根据左右边界（lb,rb）从head部分中截取相应内容
	 * @param lb 左边界
	 * @param rb 右边界
	 * @return 返回找到的字符串 或 ""
	 */
	public String getSpecifyStrHead(String lb, String rb) {
		return "";
	}

	/**
	 * 根据左右边界（lb,rb）从head部分中截取相应内容
	 * @param lb 左边界
	 * @param lbOrder　左边界出现的次数
	 * @param rb 右边界
	 * @param rbOrder 从左边界之后，右边界出现的次数
	 * @return 返回找到的字符串 或 ""
	 */
	public String getSpecifyStrHead(String lb, int lbOrder, String rb, int rbOrder) {
		return "";
	}

	/**
	 * 根据左右边界（lb,rb）从head部分中截取相应内容
	 * @param lb 左边界
	 * @param rb 右边界
	 * @param Order 出现的次数
	 * @return 返回找到的字符串 或 ""
	 */
	public String getSpecifyStrHead(String lb, String rb, int Order) {
		return "";
	}

}
