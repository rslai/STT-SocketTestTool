package org.rslai.socket.testtool.util;

/**
 * 字符串处理
 * @author rslai
 *
 */
public class StrHandle {

	/**
	 * 根据左右边界（lb,rb），截取左右边界之间的字符串
	 * @param str 待查找的字符串
	 * @param lb 左边界
	 * @param rb 右边界
	 * @return 返回找到的字符串 或 ""
	 */
	public static String substring(String str, String lb, String rb) {
		return org.rslai.socket.testtool.util.StrHandle.substring(str, lb, 1, rb, 1);
	}

	/**
	 * 根据左右边界（lb,rb）截取左右边界之间的字符串
	 * @param str 待查找的字符串
	 * @param lb 左边界
	 * @param lbOrder　左边界出现的次数
	 * @param rb 右边界
	 * @param rbOrder 从左边界之后，右边界出现的次数
	 * @return　返回找到的字符串 或 ""
	 */
	public static String substring(String str, String lb, int lbOrder, String rb, int rbOrder) {
		int begin = 0; // 开始位置
		int end = 0; // 结束位置
		int tmp = 0;
		
		// 通过循环得到开始位置
		for (int i=0; i<lbOrder; i++) {
			begin = str.indexOf(lb, begin);
			if (begin == -1) {
				return "";
			}

			begin = begin + lb.length();
		}

		if (rb.equals("")) {
			end = str.length(); // 当右边界为""则结束位置为整个字符串的长度
		} else {
			// 通过循环得到结束位置
			for (int i=0; i<rbOrder; i++) {
				if (tmp==0) {
					tmp = begin;
				}
				else {
					tmp = end + rb.length();
				}

				end = str.indexOf(rb, tmp);
				if (end == -1) {
					return "";
				}
			}
		}

		return str.substring(begin, end);
	}

	/**
	 * 根据左右边界（lb,rb），截取左右边界之间的字符串
	 * @param str 待查找的字符串
	 * @param lb 左边界
	 * @param rb 右边界
	 * @param order 取第几次出现的内容
	 * @return 返回找到的字符串 或 ""
	 */
	public static String substring(String str, String lb, String rb, int order) {
		int begin = 0;
		int end = 0;

		for (int i=order; i>=1; i--) {
			if (end!=0) {
				begin = end + rb.length();
			}

			begin = str.indexOf(lb, begin);
			if (begin == -1) {
				return "";
			}
			begin = begin + lb.length();

			if ( rb.equals("") ) {
				end = str.length();
			} else {
				end = str.indexOf(rb, begin);
			}
			if (end == -1) {
				return "";
			}
		}

		return str.substring(begin, end);
	}

}
