package org.rslai.socket.testtool.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 格式化时间日期函数
 * @author rslai
 *
 */
public class DateStr {
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-M-d'T'H:m:s'Z'");

	/**
	 * 得到 "yyyy-M-d'T'H:m:s'Z'" 格式的日期字串
	 * @return 格式化后的日期字串
	 */
	public static String getDateStr() {
		return dateFormat.format(new Date());
	}

}
