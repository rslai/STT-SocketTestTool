package org.rslai.socket.testtool.util;

import java.util.Random;

/**
 * 产生随机字符串
 * @author rslai
 *
 */
public class RandomStr {
	static Random random = new Random();

	/**
	 * 返回指定长度的随机字符串
	 * @param lenght 需要产生的随机字串的长度
	 * @return 返回随机生成的字符串 ( 大小写混杂 )
	 */
	public static String getStr(int lenght) {
		return getRandomStr(lenght);
	}

	/**
	 * 返回指定长度的大写随机字符串
	 * @param lenght 需要产生的随机字串的长度
	 * @return 返回随机生成的字符串 ( 全为大写字母 )
	 */
	public static String getStrUpperCase(int lenght) {
		return getRandomStr(lenght).toUpperCase();
	}

	/**
	 * 返回指定长度的小写随机字符串
	 * @param lenght 需要产生的随机字串的长度
	 * @return 返回随机生成的字符串 ( 全为小写字母 )
	 */
	public static String getStrLowerCase(int lenght) {
		return getRandomStr(lenght).toLowerCase();
	}

	/**
	 * 返回指定长度的随机字符串
	 * @param lenght 需要产生的随机字串的长度
	 * @return
	 */
	private static String getRandomStr(int lenght) {
		StringBuffer str = new StringBuffer();

		char[] dict = { 
				'1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 
				'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 
				'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

		for (int i=0; i<lenght; i++) {
			str.append( dict[ Math.abs(random.nextInt())%62 ] );
		}

		return str.toString();
	}

}