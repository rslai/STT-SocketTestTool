package org.rslai.socket.testtool.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ReadProperties {

	/**
	 * 读取普通键值对配置文件。输入数据源为字节流。
	 * Creation date: (2001-12-21 11:06:40)
	 * @param in java.io.InputStream
	 * @return java.util.Properties
	 */
	public static Properties loadFromProp(InputStream in) {
		try {
			Properties prop = new Properties() ;
			prop.load(in) ;
			return prop ;
		}catch(Exception ix) {
			return null ;
		}
	}

	/**
	 * 读取普通键值对配置文件。输入数据源为配置文件名。
	 * Creation date: (2001-12-21 11:06:40)
	 * @param file java.lang.String
	 * @return java.util.Properties
	 */
	public static Properties loadFromProp(String file) {
		FileInputStream fis = null ;
		try{
			fis = new FileInputStream(new File(file));
			Properties prop = loadFromProp(fis) ;
			return prop ;
		}catch(FileNotFoundException fx){
			return null ;
		}finally{
			try{
				if(fis!=null) fis.close();
			}catch(IOException ix){}
		}
	}

}
