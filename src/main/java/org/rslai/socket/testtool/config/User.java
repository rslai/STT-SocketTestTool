package org.rslai.socket.testtool.config;

import java.util.HashMap;

public class User {
	private boolean isRun = true; // 用户是否在运行标志
	private String threadName; // 用户运行的线程名

	private HashMap<String, String> info = new HashMap<String, String>(); // 用户信息

	public User(HashMap<String, String> info) {
		this.info = info;
	}

	/**
	 * 设置这个用户是否在运行标志
	 * @param IsRun
	 */
	public synchronized void setIsRun(boolean IsRun) {
		isRun = IsRun;
	}
	/**
	 * 返回这个用户是否在运行标志
	 * @return true 正在运行 , false 停止运行
	 */
	public synchronized boolean getIsRun() {
		return isRun;
	}

	/**
	 * 设置用户运行的线程名
	 * @param ThreadName
	 */
	public void setThreadName(String ThreadName) {
		threadName = ThreadName;
	}
	/**
	 * 返回用户运行的线程名
	 * @return 线程名 , 例如: [Thread0]
	 */
	public String getThreadName() {
		return threadName;
	}

	/**
	 * 设置一个用户信息，一般用在添加一个用户运行时的临时变量
	 * @param key
	 * @param value
	 */
	public void addUserInfo(String key, String value){
		info.put(key, value);
	}

	/**
	 * 设置一个用户信息，一般用在添加一个用户运行时的临时变量
	 * @param key
	 * @param value
	 */
	public void addUserInfo(String key, int value){
		info.put(key, String.valueOf(value));
	}

	/**
	 * 设置一个用户信息，一般用在添加一个用户运行时的临时变量
	 * @param key
	 * @param value
	 */
	public void addUserInfo(String key, boolean value){
		info.put(key, String.valueOf(value));
	}

	/**
	 * 返回一个用户信息（String类型），可以返回配置在User.csv中的一个值也可以返回一个用户运行时的临时变量
	 * @param key 变量名
	 * @return 返回的String型数据
	 */
	public String getUserInfoToString(String key){
		return info.get(key);
	}
	/**
	 * 返回一个用户信息（int类型），可以返回配置在User.csv中的一个值也可以返回一个用户运行时的临时变量
	 * @param key 变量名
	 * @return 返回的int型数据
	 */
	public int getUserInfoToInt(String key){
		String str = getUserInfoToString(key);
		if (str != null) {
			return Integer.parseInt(str);
		} else {
			return 0;
		}
	}
	/**
	 * 返回一个用户信息（boolean类型），可以返回配置在User.csv中的一个值也可以返回一个用户运行时的临时变量
	 * @param key 变量名
	 * @return 返回的boolean型数据
	 */
	public boolean getUserInfoToBoolean(String key){
		String str = getUserInfoToString(key);
		if (str != null) {
			return Boolean.parseBoolean(str);
		} else {
			return false;
		}
	}

	/**
	 * 删除一个用户信息，一般用在删除一个用户运行时的临时变量
	 * @param key 变量名
	 */
	public void removeUserInfo(String key){
		info.remove(key);
	}

}