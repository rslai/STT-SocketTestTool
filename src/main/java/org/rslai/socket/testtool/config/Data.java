package org.rslai.socket.testtool.config;

import java.util.HashMap;
import org.apache.log4j.Logger;

public class Data {
	static Logger logger = Logger.getLogger(Data.class);

	private static HashMap<String, String> keyValues = new HashMap<String, String>(); // key value形式的配置参数
	private static HashMap<String, HashMap<String, HashMap<String, String>>> tables = new HashMap<String, HashMap<String, HashMap<String, String>>>(); // 表的形式的配置信息

	/**
	 * 添加一个key value的配置参数
	 * @param key 变量名
	 * @param value 变量内容
	 * @param desc 变量作用说明
	 */
	public static void addKeyValue(String key, String value, String desc) {
		keyValues.put(key, value);

		logger.warn(desc + "(" + key + "): " + value);
	}

	/**
	 * 返回一个字符串型的key value的配置参数的值
	 * @param key 变量名
	 * @return 返回的String型数据
	 */
	public static String getKeyValueToString(String key) {
		return keyValues.get(key);
	}
	/**
	 * 返回一个int型的key value的配置参数的值
	 * @param key 变量名
	 * @return 返回的int型数据
	 */
	public static int getKeyValueToInt(String key) {
		String str = getKeyValueToString(key);
		if (str != null) {
			return Integer.parseInt(str);
		} else {
			return 0;
		}
	}
	/**
	 * 返回一个int型的key value的配置参数的值
	 * @param key 变量名
	 * @return 返回的boolean型数据
	 */
	public static boolean getKeyValueToBoolean(String key){
		String str = getKeyValueToString(key);
		if (str != null) {
			return Boolean.parseBoolean(str);
		} else {
			return false;
		}
	}

	/**
	 * 添加一个表的形式的配置信息（添加一个表）
	 * @param tableName 表名
	 * @param table 表内容
	 */
	public static void addTable(String tableName, HashMap<String, HashMap<String, String>> table) {
		tables.put(tableName, table);

		logger.info(tableName + ": " + table.keySet());
	}

	/**
	 * 返回表形式配置信息的一个表（返回一个表）
	 * @param tableName
	 * @return 返回的一个表数据
	 */
	public static HashMap<String, HashMap<String, String>> getTable(String tableName) {
		return tables.get(tableName);
	}

	/**
	 * 返回表形式配置信息的一行（返回一个表中的一行记录）
	 * @param tableName tableName 表名
	 * @param rowID 行id（在isRun字段后的第一个字段内容）
	 * @return 返回表中的一行数据
	 */
	public static HashMap<String, String> getTableRow(String tableName, String rowID) {
		return getTable(tableName).get(rowID);
	}

}
