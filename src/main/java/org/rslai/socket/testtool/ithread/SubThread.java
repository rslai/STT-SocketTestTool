package org.rslai.socket.testtool.ithread;

/**
 * 子线程（抽象类）
 * @author Administrator
 *
 */
public abstract class SubThread extends Thread {
	protected MainThread mainThread;

	/**
	 * 构造函数
	 * @param mainThread 主线程
	 */
	public SubThread(MainThread mainThread) {
		setDaemon(true); // 守护线程，随着main线程结束而结束

		this.mainThread = mainThread;
	}

	/**
	 * overrides java.lang.Thread.run
	 */
	abstract public void run();
}
