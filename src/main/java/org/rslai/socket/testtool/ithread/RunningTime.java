package org.rslai.socket.testtool.ithread;
import org.apache.log4j.Logger;

/**
 * 运行时间，运行一定时间后结束运行
 * @author rslai
 *
 */
public class RunningTime extends SubThread {
	static Logger logger = Logger.getLogger(RunningTime.class);

	private int sec; // 运行的秒数

	/**
	 * 构造函数
	 * @param mainThread 主线程
	 * @param sec 运行的秒数
	 */
	public RunningTime(MainThread mainThread, int sec) {
		super(mainThread);

		this.sec = sec; // 运行的秒数
	}

	/**
	 * overrides java.lang.Thread.run
	 */
	public void run() {
		try {
			yield();
			sleep(1000 * sec);

			if (mainThread.getIsRun()) {
				mainThread.setIsRun(false);
				mainThread.exit();
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception: " + e);
		}
	}

}