package org.rslai.socket.testtool.ithread;

/**
 * 主线程（抽象类）
 * @author rslai
 *
 */
public abstract class MainThread extends Thread {
	protected org.rslai.socket.testtool.config.User user;

	/**
	 * 构造函数
	 * @param user user信息
	 */
	public MainThread(org.rslai.socket.testtool.config.User user) {
		this.user = user;
		this.user.setThreadName("[" + this.getName() + "] "); // 设置当前线程名
	}

	/**
	 * overrides java.lang.Thread.run
	 */
	abstract public void run();

	/**
	 * 设置是否正在运行标记
	 * @param IsRun true 正在运行, false 已经停止运新
	 */
	public synchronized void setIsRun(boolean IsRun) {
		user.setIsRun(IsRun);
	}

	/**
	 * 取得是否正在运行标记
	 * @return true 正在运行, false 停止运行
	 */
	public synchronized boolean getIsRun() {
		return user.getIsRun();
	}

	/**
	 * 结束主线程和其所有子线程
	 */
	abstract public void exit();

}