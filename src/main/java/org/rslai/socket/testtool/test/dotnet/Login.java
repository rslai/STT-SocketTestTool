package org.rslai.socket.testtool.test.dotnet;

import org.apache.log4j.*;
import org.rslai.socket.testtool.config.User;
import org.rslai.socket.testtool.isocket.*;

public class Login {
	static Logger logger = Logger.getLogger(Login.class);

	private StringBuffer sendMsg = new StringBuffer();

	public void execute(ISocket iSocket, User user) throws Exception {
		if (user.getIsRun()) {
			logger.info("登录 - 发送第一个包");
			packOne(iSocket, user);
		}

		if (user.getIsRun()) {
			logger.info("登录 - 发送第二个包");
			packTwo(iSocket, user);
		}
	}
	
	public void packOne(ISocket iSocket, User user) throws Exception{
		sendMsg = new StringBuffer();

		sendMsg.append( ".NET" );
		sendMsg.append( "^01^00^00^00^00^00^F9^07^00^00^04^00^01^01^29^00^00^00" );
		sendMsg.append( "tcp://" + org.rslai.socket.testtool.config.Data.getKeyValueToString("SHLRserver") + ":" + org.rslai.socket.testtool.config.Data.getKeyValueToInt("SHLRport") + "/CommService.rem" );
		sendMsg.append( "^06^00^01^01^18^00^00^00" );
		sendMsg.append( "application/octet-stream" );
		sendMsg.append( "^00^00" );

		iSocket.send(org.rslai.socket.testtool.util.TranHandle.strToArrayByte(sendMsg.toString()));

		sendMsg = new StringBuffer();
		sendMsg.append( "^00^01^00^00^00^FF^FF^FF^FF^01^00^00^00^00^00^00^00^15^14^00^00^00^12^0B" );
		sendMsg.append( "Communicate^12fCE.Communication.ICommunication, ICommunication, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" ); 
		sendMsg.append( "^10^01^00^00^00^04^00^00^00^09^02^00^00^00^09^03^00^00^00^09^04^00^00^00^06^05^00^00^00^00^0C^06^00^00^00" );
		sendMsg.append( "EICommunication, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" );
		sendMsg.append( "^05^02^00^00^00^20" );
		sendMsg.append( "CE.Communication.EnumMajorOpCode" );
		sendMsg.append( "^01^00^00^00^07" );
		sendMsg.append( "value__" );
		sendMsg.append( "^00^0E^06^00^00^00^12^00^05^03^00^00^00^20" );
		sendMsg.append( "CE.Communication.EnumMinorOpCode" );
		sendMsg.append( "^01^00^00^00^07" );
		sendMsg.append( "value__" );
		sendMsg.append( "^00^02^06^00^00^00^00^04^04^00^00^00^1C" );
		sendMsg.append( "System.Collections.ArrayList" );
		sendMsg.append( "^03^00^00^00^06" );
		sendMsg.append( "_items^05_size^08_version" );
		sendMsg.append( "^05^00^00^08^08^09^07^00^00^00^01^00^00^00^01^00^00^00^10^07^00^00^00^04^00^00^00^09^08^00^00^00^0D^03^0C^09^00^00^00" );
		sendMsg.append( "@DataTable, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" );
		sendMsg.append( "^0C^0A^00^00^00" );
		sendMsg.append( "NSystem.Data, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" );
		sendMsg.append( "^05^08^00^00^00" );
		sendMsg.append( "%CE.Communication.DataTable.LoginTable" );
		sendMsg.append( "^02^00^00^00^09" );
		sendMsg.append( "whereData^06dtData^03^04^0FSystem.Object[]^15System.Data.DataTable" );
		sendMsg.append( "^0A^00^00^00^09^00^00^00^09^0B^00^00^00^09^0C^00^00^00^10^0B^00^00^00^01^00^00^00^06^0D^00^00^00^28" );
		sendMsg.append( " WHERE CACCOUNT = '" + user.getUserInfoToString("UserID") + "' AND CPWD = '" + user.getUserInfoToString("Password") + "'" );
		sendMsg.append( "^05^0C^00^00^00^15" );
		sendMsg.append( "System.Data.DataTable" );
		sendMsg.append( "^03^00^00^00^19" );
		sendMsg.append( "DataTable.RemotingVersion^09XmlSchema^0BXmlDiffGram^03^01^01^0ESystem.Version" );
		sendMsg.append( "^0A^00^00^00^09^0E^00^00^00^06^0F^00^00^00^A1^07" );
		sendMsg.append( "<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n" );
		sendMsg.append( "<xs:schema xmlns=\"\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:msdata=\"urn:schemas-microsoft-com:xml-msdata\">\r\n" );
		sendMsg.append( "  <xs:element name=\"Table1\">\r\n" );
		sendMsg.append( "    <xs:complexType>\r\n" );
		sendMsg.append( "      <xs:sequence>\r\n" );
		sendMsg.append( "        <xs:element name=\"CACCOUNT\" type=\"xs:string\" msdata:targetNamespace=\"\" minOccurs=\"0\" />\r\n" );
		sendMsg.append( "        <xs:element name=\"CUSERNAME\" type=\"xs:string\" msdata:targetNamespace=\"\" minOccurs=\"0\" />\r\n" );
		sendMsg.append( "        <xs:element name=\"CPWD\" type=\"xs:string\" msdata:targetNamespace=\"\" minOccurs=\"0\" />\r\n" );
		sendMsg.append( "        <xs:element name=\"NRIGHTS\" type=\"xs:decimal\" msdata:targetNamespace=\"\" minOccurs=\"0\" />\r\n" );
		sendMsg.append( "      </xs:sequence>\r\n" );
		sendMsg.append( "    </xs:complexType>\r\n" );
		sendMsg.append( "  </xs:element>\r\n" );
		sendMsg.append( "  <xs:element name=\"tmpDataSet\" msdata:IsDataSet=\"true\" msdata:MainDataTable=\"Table1\" msdata:UseCurrentLocale=\"true\">\r\n" );
		sendMsg.append( "    <xs:complexType>\r\n" );
		sendMsg.append( "      <xs:choice minOccurs=\"0\" maxOccurs=\"unbounded\" />\r\n" );
		sendMsg.append( "    </xs:complexType>\r\n" );
		sendMsg.append( "  </xs:element>\r\n</xs:schema>" );
		sendMsg.append( "^06^10^00^00^00^80^01" );
		sendMsg.append( "<diffgr:diffgram xmlns:msdata=\"urn:schemas-microsoft-com:xml-msdata\" xmlns:diffgr=\"urn:schemas-microsoft-com:xml-diffgram-v1\" />" );
		sendMsg.append( "^04^0E^00^00^00^0E" );
		sendMsg.append( "System.Version^04^00^00^00^06_Major^06_Minor^06_Build^09_Revision" );
		sendMsg.append( "^00^00^00^00^08^08^08^08^02^00^00^00^00^00^00^00^FF^FF^FF^FF^FF^FF^FF^FF^0B" );

		iSocket.send(org.rslai.socket.testtool.util.TranHandle.strToArrayByte(sendMsg.toString()));

		ReceiveData rData = iSocket.receive();

		if (rData.getBody().indexOf("Exception") != -1) {
			logger.error("Exit: Receive Exception");
			user.setIsRun(false);
		}
	}
	
	public void packTwo(ISocket iSocket, User user) throws Exception{
		sendMsg = new StringBuffer();

		sendMsg.append( ".NET" );
		sendMsg.append( "^01^00^00^00^00^00^D4^09^00^00^04^00^01^01^29^00^00^00" );
		sendMsg.append( "tcp://" + org.rslai.socket.testtool.config.Data.getKeyValueToString("SHLRserver") + ":" + org.rslai.socket.testtool.config.Data.getKeyValueToInt("SHLRport") + "/CommService.rem" );
		sendMsg.append( "^06^00^01^01^18^00^00^00" );
		sendMsg.append( "application/octet-stream" );
		sendMsg.append( "^00^00" );

		iSocket.send(org.rslai.socket.testtool.util.TranHandle.strToArrayByte(sendMsg.toString()));

		sendMsg = new StringBuffer();

		sendMsg.append( "^00^01^00^00^00^FF^FF^FF^FF^01^00^00^00^00^00^00^00^15^14^00^00^00^12^0B" );
		sendMsg.append( "Communicate^12fCE.Communication.ICommunication, ICommunication, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" ); 
		sendMsg.append( "^10^01^00^00^00^04^00^00^00^09^02^00^00^00^09^03^00^00^00^09^04^00^00^00^06^05^00^00^00^00^0C^06^00^00^00" );
		sendMsg.append( "EICommunication, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" );
		sendMsg.append( "^05^02^00^00^00^20" );
		sendMsg.append( "CE.Communication.EnumMajorOpCode" );
		sendMsg.append( "^01^00^00^00^07" );
		sendMsg.append( "value__" );
		sendMsg.append( "^00^0E^06^00^00^00^11^00^05^03^00^00^00^20" );
		sendMsg.append( "CE.Communication.EnumMinorOpCode" );
		sendMsg.append( "^01^00^00^00^07" );
		sendMsg.append( "value__" );
		sendMsg.append( "^00^02^06^00^00^00^00^04^04^00^00^00^1C" );
		sendMsg.append( "System.Collections.ArrayList" );
		sendMsg.append( "^03^00^00^00^06" );
		sendMsg.append( "_items^05_size^08_version" );
		sendMsg.append( "^05^00^00^08^08^09^07^00^00^00^01^00^00^00^01^00^00^00^10^07^00^00^00^04^00^00^00^09^08^00^00^00^0D^03^0C^09^00^00^00" );
		sendMsg.append( "@DataTable, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" );
		sendMsg.append( "^0C^0A^00^00^00" );
		sendMsg.append( "EDatabaseAccess, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" );
		sendMsg.append( "^0C^0B^00^00^00" );
		sendMsg.append( "NSystem.Data, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" );
		sendMsg.append( "^05^08^00^00^00" );
		sendMsg.append( "'CE.Communication.DataTable.CNINFO_TABLE" );
		sendMsg.append( "^06^00^00^00^09" );
		sendMsg.append( "whereData^06Schema^09tablename^08strwhere^06strSql^06dtData" );
		sendMsg.append( "^03^04^01^01^01^04^0C" );
		sendMsg.append( "System.Array CE.DatabaseAccess.EnumSchemaName" );
		sendMsg.append( "^0A^00^00^00^15" );
		sendMsg.append( "System.Data.DataTable" );
		sendMsg.append( "^0B^00^00^00^09^00^00^00^0A^05^F4^FF^FF^FF^20" );
		sendMsg.append( "CE.DatabaseAccess.EnumSchemaName^01^00^00^00^07value__" );
		sendMsg.append( "^00^08^0A^00^00^00^00^00^00^00^06^0D^00^00^00^0A" );
		sendMsg.append( "TBSYSPARAM" );
		sendMsg.append( "^06^0E^00^00^00" );
		sendMsg.append( "Dwhere CNAME = 'Version' or CNAME = 'DownloadsPath' order by id  desc" );
		sendMsg.append( "^0A^09^0F^00^00^00^05^0F^00^00^00^15" );
		sendMsg.append( "System.Data.DataTable^03^00^00^00^19DataTable.RemotingVersion^09XmlSchema^0BXmlDiffGram^03^01^01^0E" );
		sendMsg.append( "System.Version^0B^00^00^00^09^10^00^00^00^06^11^00^00^00^8C^09" );
		sendMsg.append( "<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n" );
		sendMsg.append( "<xs:schema xmlns=\"\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:msdata=\"urn:schemas-microsoft-com:xml-msdata\">\r\n" );
		sendMsg.append( "  <xs:element name=\"tbsysparam\">\r\n" );
		sendMsg.append( "    <xs:complexType>\r\n" );
		sendMsg.append( "      <xs:sequence>\r\n" );
		sendMsg.append( "        <xs:element name=\"CNAME\" type=\"xs:string\" msdata:targetNamespace=\"\" minOccurs=\"0\" />\r\n" );
		sendMsg.append( "        <xs:element name=\"CVALUE\" type=\"xs:string\" msdata:targetNamespace=\"\" minOccurs=\"0\" />\r\n" );
		sendMsg.append( "        <xs:element name=\"CMEMO\" type=\"xs:string\" msdata:targetNamespace=\"\" minOccurs=\"0\" />\r\n" );
		sendMsg.append( "        <xs:element name=\"DDATE\" type=\"xs:dateTime\" msdata:targetNamespace=\"\" minOccurs=\"0\" />\r\n" );
		sendMsg.append( "        <xs:element name=\"ID\" type=\"xs:decimal\" msdata:targetNamespace=\"\" />\r\n" );
		sendMsg.append( "      </xs:sequence>\r\n" );
		sendMsg.append( "    </xs:complexType>\r\n" );
		sendMsg.append( "  </xs:element>\r\n" );
		sendMsg.append( "  <xs:element name=\"NewDataSet\" msdata:IsDataSet=\"true\" msdata:MainDataTable=\"tbsysparam\" msdata:UseCurrentLocale=\"true\">\r\n" );
		sendMsg.append( "    <xs:complexType>\r\n" );
		sendMsg.append( "      <xs:choice minOccurs=\"0\" maxOccurs=\"unbounded\" />\r\n" );
		sendMsg.append( "    </xs:complexType>\r\n" );
		sendMsg.append( "    <xs:unique name=\"Constraint1\" msdata:PrimaryKey=\"true\">\r\n" );
		sendMsg.append( "      <xs:selector xpath=\".//tbsysparam\" />\r\n" );
		sendMsg.append( "      <xs:field xpath=\"ID\" />\r\n" );
		sendMsg.append( "    </xs:unique>\r\n  </xs:element>\r\n</xs:schema>" );
		sendMsg.append( "^06^12^00^00^00^80^01" );
		sendMsg.append( "<diffgr:diffgram xmlns:msdata=\"urn:schemas-microsoft-com:xml-msdata\" xmlns:diffgr=\"urn:schemas-microsoft-com:xml-diffgram-v1\" />" );
		sendMsg.append( "^04^10^00^00^00^0E" );
		sendMsg.append( "System.Version^04^00^00^00^06_Major^06_Minor^06_Build^09_Revision" );
		sendMsg.append( "^00^00^00^00^08^08^08^08^02^00^00^00^00^00^00^00^FF^FF^FF^FF^FF^FF^FF^FF^0B" );

		iSocket.send(org.rslai.socket.testtool.util.TranHandle.strToArrayByte(sendMsg.toString()));

		ReceiveData rData = iSocket.receive();

		// 判断如果收到 Exception 设置不再运行标志
		if (rData.getBody().indexOf("Exception") != -1) {
			logger.error("Exit: Receive Exception");
			user.setIsRun(false);
		}
	}
}
