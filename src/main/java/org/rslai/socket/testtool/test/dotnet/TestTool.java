package org.rslai.socket.testtool.test.dotnet;

import org.apache.log4j.*;
import org.rslai.socket.testtool.config.User;
import org.rslai.socket.testtool.isocket.*;
import org.rslai.socket.testtool.ithread.*;
import java.util.Iterator;
import java.util.LinkedList;

public class TestTool extends MainThread{
	private ISocket localSocket;

	static Logger logger = Logger.getLogger(TestTool.class);

	public TestTool(User user) {
		super(user);
	}

	public void run() {
		try {

			logger.warn(user.getThreadName() + " START. userID=" + user.getUserInfoToString("ID"));

			yield();

			/*
			 * 创建连接
			 */
			logger.warn("创建连接 Begin");

			ISocketFactory iSocketFactory = new ISocketFactory(org.rslai.socket.testtool.config.Data.getKeyValueToString("SHLRserver"), org.rslai.socket.testtool.config.Data.getKeyValueToInt("SHLRport"), ISocket.UTF8);
			ISocket SHLRsocket = iSocketFactory.getISocket("org.rslai.socket.testtool.isocket.ISocketDotNet");

			logger.warn("创建连接 End");

			/*
			 * 登录
			 */
			if (user.getIsRun()) {
				logger.warn("登录 Begin");

				Login login = new Login();
				login.execute(SHLRsocket, user);

				logger.warn("登录 End");
			}

			/*
			 * 在这之后按照以上方式添加其它功能
			 */

			// 循环等待运行时间
			while(true){
				if (!this.getIsRun()) {
					break; // 运行时间到退出循环
				}
			}

		} catch(Exception e) {
			e.printStackTrace();
			logger.error("Exception: " + e);
		}
		
	}

	/**
	 * 结束主线程和其所有子线程
	 */
	public void exit() {
	}

	public static void main(String args[]) {
		BasicConfigurator.configure();

		// 装载服务器配置及用户数据
		new org.rslai.socket.testtool.config.LoadConfig("./datas/Config.xml");

		Iterator<String> it = org.rslai.socket.testtool.config.Data.getTable("User").keySet().iterator();
		LinkedList<Thread> vUsers = new LinkedList<Thread>();
		while (it.hasNext()) {
			org.rslai.socket.testtool.config.User user = new org.rslai.socket.testtool.config.User(org.rslai.socket.testtool.config.Data.getTableRow("User", it.next()));
			MainThread vUser = new TestTool( user );
			vUsers.addLast(vUser);
			vUser.start();

			new RunningTime(vUser, 15).start(); // 启动运行时间线程
		}
	}
}