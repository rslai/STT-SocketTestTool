package org.rslai.socket.testtool.test.p2p;

import org.apache.log4j.*;
import org.rslai.socket.testtool.isocket.*;

import java.util.Iterator;
import java.util.LinkedList;

public class TestTool extends Thread {

	private String threadName = this.getName();

	private org.rslai.socket.testtool.config.User user;
	private ISocket localSocket;
	int i=1;

	static Logger logger = Logger.getLogger(TestTool.class);

	public TestTool(org.rslai.socket.testtool.config.User User) {
		user = User;
		user.setThreadName("[" + this.getName() + "] ");
	}
	
	public void run() {
		try {

			logger.warn("[" + threadName + "] START. userID=" + user.getUserInfoToString("ID"));

			yield();

			/*
			 * 登录 动力 IM, SIP 服务
			 */
			logger.warn("发送第一个包");

			ISocketFactory iSocketFactory = new ISocketFactory(org.rslai.socket.testtool.config.Data.getKeyValueToString("P2Pserver"), org.rslai.socket.testtool.config.Data.getKeyValueToInt("P2Pport"), ISocket.UTF8);
			ISocket SIPsocket = iSocketFactory.getISocket("org.rslai.socket.testtool.isocket.ISocketP2P");

			// Send1
			org.rslai.socket.testtool.test.p2p.Send1 send1 = new org.rslai.socket.testtool.test.p2p.Send1();
			send1.execute(SIPsocket, user);

		}

		catch(Exception e) {
			e.printStackTrace();
			logger.error("Exception: " + e);
		}

	}

	public synchronized void SetIsRun(boolean IsRun) {
		user.setIsRun(IsRun);
	}
	public synchronized boolean GetIsRun() {
		return user.getIsRun();
	}

	public static void main(String args[]) {
		BasicConfigurator.configure();

		// 装载服务器配置及用户数据
		new org.rslai.socket.testtool.config.LoadConfig("./datas/Config.xml");

		Iterator<String> it = org.rslai.socket.testtool.config.Data.getTable("User").keySet().iterator();
		LinkedList<Thread> vUsers = new LinkedList<Thread>();
		while (it.hasNext()) {
			org.rslai.socket.testtool.config.User user = new org.rslai.socket.testtool.config.User(org.rslai.socket.testtool.config.Data.getTableRow("User", it.next()));
			Thread vUser = new TestTool( user );
			vUsers.addLast(vUser);
			vUser.start();
		}
	}
}

