package org.rslai.socket.testtool.test.im;

import org.apache.log4j.*;
import org.rslai.socket.testtool.isocket.*;

import java.util.Iterator;
import java.util.LinkedList;

public class TestTool extends Thread {

	private String threadName = this.getName();

	private org.rslai.socket.testtool.config.User user;
	private ISocket localSocket;
	int i=1;

	static Logger logger = Logger.getLogger(TestTool.class);

	public TestTool(org.rslai.socket.testtool.config.User User) {
		user = User;
		user.setThreadName("[" + this.getName() + "] ");
	}
	
	public void run() {
		try {

			logger.warn("[" + threadName + "] START. userID=" + user.getUserInfoToString("ID"));

			yield();

			/*
			 * 登录 动力 IM, SIP 服务
			 */
			logger.warn("登录 Begin - SIP");

			ISocketFactory iSocketFactory = new ISocketFactory(org.rslai.socket.testtool.config.Data.getKeyValueToString("SIPserver"), org.rslai.socket.testtool.config.Data.getKeyValueToInt("SIPport"), ISocket.UTF8);
			ISocket SIPsocket = iSocketFactory.getISocket("org.rslai.socket.testtool.isocket.ISocketIM");

			// SIP REGISTER
			org.rslai.socket.testtool.test.im.REGISTER sipREGISTER = new org.rslai.socket.testtool.test.im.REGISTER();
			sipREGISTER.execute(SIPsocket, user);

//			Login login = new Login();
//			login.execute(localSocket, user);
			
/*
			// SIP PUBLISH
			SIPserver.PUBLISH sipPUBLISH = new SIPserver.PUBLISH();
			sipPUBLISH.execute(SIPsocket, user, parse);

			// SIP SUBSCRIBE
			SIPserver.SUBSCRIBE sipSUBSCRIBE = new SIPserver.SUBSCRIBE();
			sipSUBSCRIBE.execute(SIPsocket, user, parse);

			// SIP SIPOK
			SIPserver.SIPOK sipSIPOK = new SIPserver.SIPOK();
			sipSIPOK.execute(SIPsocket, user, parse);

			logger.warn("登录 End");

			logger.warn("发送消息 Begin - SIP");

			//SIP MESSAGE
			SIPserver.MESSAGE sipMESSAGE = new SIPserver.MESSAGE();
			sipMESSAGE.execute(SIPsocket, user, parse);

			logger.warn("发送消息 End");*/
		}

		catch(Exception e) {
			e.printStackTrace();
			logger.error("Exception: " + e);
		}

	}

	public synchronized void SetIsRun(boolean IsRun) {
		user.setIsRun(IsRun);
	}
	public synchronized boolean GetIsRun() {
		return user.getIsRun();
	}

	public static void main(String args[]) {
		BasicConfigurator.configure();

		// 装载服务器配置及用户数据
		new org.rslai.socket.testtool.config.LoadConfig("./datas/Config.xml");

		Iterator<String> it = org.rslai.socket.testtool.config.Data.getTable("User").keySet().iterator();
		LinkedList<Thread> vUsers = new LinkedList<Thread>();
		while (it.hasNext()) {
			org.rslai.socket.testtool.config.User user = new org.rslai.socket.testtool.config.User(org.rslai.socket.testtool.config.Data.getTableRow("User", it.next()));
			Thread vUser = new TestTool( user );
			vUsers.addLast(vUser);
			vUser.start();
		}
	}
}

