package org.rslai.socket.testtool.test.CXTV;

import org.apache.log4j.*;
import org.rslai.socket.testtool.isocket.*;
import org.rslai.socket.testtool.config.*;
import org.rslai.socket.testtool.util.*;

/**
 * 登陆
 * @author wb
 *
 */
public class send1 {
	static Logger logger = Logger.getLogger(send1.class);

	private StringBuffer sendMsg = new StringBuffer();

	public void execute(ISocket iSocket, User user) throws Exception {

		logger.info("登录 - 发送第一个包");
		packOne(iSocket, user);

	}
	
	public void packOne(ISocket iSocket, User user) throws Exception{
		sendMsg = new StringBuffer();		

		logger.warn("发送第一个包 Begin");
		
		sendMsg.append( "^E0^06^00^00^1A^00^00^00^C5^04^E8^03^00^00^00^00^00^00^E5^00^01^93^04^30^2C^32^34^30^00^00^54^00^01^00" );

		iSocket.send(TranHandle.strToArrayByte(sendMsg.toString()));
	
		sendMsg = new StringBuffer();
		sendMsg.append( "" );

		iSocket.send(TranHandle.strToArrayByte(sendMsg.toString()));

		logger.warn("发送第一个包 End");

		logger.warn("接收第一个包 Begin");

		ReceiveData rData = iSocket.receive();

		logger.warn("接收第一个包 End");

		System.out.println(rData.getBody());
	}

}
