package org.rslai.socket.testtool.test.im;

import org.apache.log4j.*;
import org.rslai.socket.testtool.config.User;
import org.rslai.socket.testtool.isocket.*;
import org.rslai.socket.testtool.md5.SIPcompute;

public class REGISTER {
	static Logger logger = Logger.getLogger(REGISTER.class);

	private StringBuffer sendMsg = new StringBuffer();
	private StringBuffer sendMsgBody = new StringBuffer();

	public void execute(ISocket iSocket, User user) throws Exception{

		logger.info("SIP REGISTER");

		/*
		 * 第一次发送 REGISTER 取得挑战信息
		 */

		user.addUserInfo("BrREGISTER", "z9hG4bK-d8754z-" + org.rslai.socket.testtool.util.RandomStr.getStrUpperCase(16) + "-1---d8754z-");
		user.addUserInfo("RiREGISTER", org.rslai.socket.testtool.util.RandomStr.getStrLowerCase(16));
		user.addUserInfo("RiREGISTER", org.rslai.socket.testtool.util.RandomStr.getStrLowerCase(8));
		user.addUserInfo("RiREGISTER", org.rslai.socket.testtool.util.RandomStr.getStrLowerCase(43));
		user.addUserInfo("RegisterTime", "0");

		// 构造向服务器请求体信息
		sendMsgBody.append( "" );

		// 构造向服务器请求头信息
		sendMsg.append( "REGISTER sip:myce.net.cn SIP/2.0\r\n" );
		sendMsg.append("Via: SIP/2.0/TCP " + org.rslai.socket.testtool.config.Data.getKeyValueToString("SIPserver") + ":5070;");
		sendMsg.append( "branch=" + user.getUserInfoToString("BrREGISTER") + ";rport\r\n" );
		sendMsg.append( "Max-Forwards: 70\r\n" );
		sendMsg.append( "Contact: <sip:" + user.getUserInfoToString("UserID") + "@" + org.rslai.socket.testtool.config.Data.getKeyValueToString("SIPserver") + ":5070;rinstance="+user.getUserInfoToString("RiREGISTER")+";transport=TCP>\r\n" );
		sendMsg.append( "To: <sip:" + user.getUserInfoToString("UserID") + "@" + user.getUserInfoToString("Domain") + ">\r\n" );
		sendMsg.append( "From: <sip:" + user.getUserInfoToString("UserID") + "@" + user.getUserInfoToString("Domain") + ">;tag="+user.getUserInfoToString("FrREGISTER")+"\r\n" );
		sendMsg.append( "Call-ID: " + user.getUserInfoToString("CallIdREGISTER") + ".\r\n" );
		sendMsg.append( "CSeq: " + user.getUserInfoToInt("RegisterTime") + " REGISTER\r\n" );
		sendMsg.append( "Expires: 3600\r\n" );
		sendMsg.append("Allow: INVITE, ACK, CANCEL, BYE, OPTIONS, MESSAGE, NOTIFY\r\n");
		sendMsg.append("User-Agent: deskim_app\r\n");
		sendMsg.append("Content-Length: " + sendMsgBody.length() + "\r\n\r\n");
		sendMsg.append( sendMsgBody );

		iSocket.send(sendMsg.toString());

		user.removeUserInfo("BrREGISTER");

		ReceiveData rData = iSocket.receive();

		// 判断返回数据第一行是否返回 "SIP/2.0 407 Proxy Authentication Required"
		String state = rData.getSpecifyStrHead("", "\r\n");
		if (!state.equals("SIP/2.0 407 Proxy Authentication Required")) logger.error(state);

		/*
		 * 第二次发送 REGISTER 将计算后的 MD5 发回服务器
		 */

		// 取得 SIP 服务器的挑战信息
		String nonce = rData.getSpecifyStrHead("Proxy-Authenticate: Digest nonce=\"", "\"");

		// 根据挑战信息计算 MD5 码
		SIPcompute SIPcompute = new SIPcompute();
		String authMD5 = SIPcompute.compute(user.getUserInfoToString("UserID"), user.getUserInfoToString("Domain"), user.getUserInfoToString("Password"), nonce);
		sendMsg = new StringBuffer();
		sendMsgBody = new StringBuffer();

		user.addUserInfo("BrREGISTER1", "z9hG4bK-d8754z-" + org.rslai.socket.testtool.util.RandomStr.getStrUpperCase(16) + "-1---d8754z-");


		// 构造向服务器请求体信息
		sendMsgBody.append( "" );

		user.addUserInfo("RegisterTime", (user.getUserInfoToInt("RegisterTime")+1)+"");

		// 构造向服务器请求头信息
		sendMsg.append( "REGISTER sip:" + user.getUserInfoToString("Domain") + " SIP/2.0\r\n" );
		sendMsg.append( "Via: SIP/2.0/TCP " + org.rslai.socket.testtool.config.Data.getKeyValueToString("SIPserver") + ":5070;branch="+ user.getUserInfoToString("BrREGISTER1") +";rport\r\n" );
		sendMsg.append("Max-Forwards: 70\r\n");
		sendMsg.append( "Contact: <sip:" + user.getUserInfoToString("UserID") + "@" + org.rslai.socket.testtool.config.Data.getKeyValueToString("SIPserver") + ":5070;rinstance="+user.getUserInfoToString("RiREGISTER")+";transport=tcp>\r\n" );
		sendMsg.append( "To: <sip:" + user.getUserInfoToString("UserID") + "@" + user.getUserInfoToString("Domain") + ">\r\n" );
		sendMsg.append( "From: <sip:" + user.getUserInfoToString("UserID") + "@" + user.getUserInfoToString("Domain") + ">;tag="+user.getUserInfoToString("FrREGISTER")+"\r\n" );
		sendMsg.append( "Call-ID: " + user.getUserInfoToString("CallIdREGISTER") + ".\r\n" );
		sendMsg.append( "CSeq: " + user.getUserInfoToInt("RegisterTime") + " REGISTER\r\n" );
		sendMsg.append( "Expires: 3600\r\n" );
		sendMsg.append( "Allow: INVITE, ACK, CANCEL, BYE, OPTIONS, MESSAGE, NOTIFY\r\n");
		sendMsg.append( "Proxy-Authorization: Digest username=\""+user.getUserInfoToString("UserID")+"\",realm=\""+user.getUserInfoToString("Domain")+"\",nonce=\""+nonce+"\",uri=\"sip:"+user.getUserInfoToString("Domain")+"\",response=\""+ authMD5 + "\",algorithm=MD5\r\n");
		sendMsg.append( "User-Agent: deskim_app\r\n" );
		sendMsg.append( "Content-Length: " + sendMsgBody.length() + "\r\n\r\n" );
		sendMsg.append( sendMsgBody );

		iSocket.send(sendMsg.toString());

		user.removeUserInfo("BrREGISTER1");
		user.removeUserInfo("RiREGISTER");
		user.removeUserInfo("FrREGISTER");
		user.removeUserInfo("CallIdREGISTER");

		rData =	iSocket.receive();

		// 判断返回数据第一行是否返回 “SIP/2.0 200 OK”
		state = rData.getSpecifyStrHead("", "\r\n");
		if (!state.equals("SIP/2.0 200 OK")) logger.error(state);

	}
}
