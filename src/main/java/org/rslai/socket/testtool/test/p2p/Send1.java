package org.rslai.socket.testtool.test.p2p;

import org.apache.log4j.*;
import org.rslai.socket.testtool.config.User;
import org.rslai.socket.testtool.isocket.*;
import org.rslai.socket.testtool.md5.SIPcompute;
import org.rslai.socket.testtool.util.TranHandle;

public class Send1 {
	static Logger logger = Logger.getLogger(Send1.class);

	private StringBuffer sendMsg = new StringBuffer();
	private StringBuffer sendMsgBody = new StringBuffer();

	public void execute(ISocket iSocket, User user) throws Exception{
		logger.warn("登录 - 发送第一个包 Begin");

		if (user.getIsRun()) {
			logger.info("登录 - 发送第一个包");
			packOne(iSocket, user);
		}


	}

	private void packOne(ISocket iSocket, User user) throws Exception{
		sendMsg = new StringBuffer();		

		logger.warn("send1 - 发送第一个包 Begin");
		
		sendMsg.append( "^05^2F^00^00^00^3C^62^74^74^61^73^6B^3E^3C^74^61^73^6B^3E^3C^70^20^68^6F^73^74^69^64^3D^22^61^62^63^22^2F^3E^3C^2F^74^61^73^6B^3E^3C^2F^62^74^74^61^73^6B^3E" );

		iSocket.send(TranHandle.strToArrayByte(sendMsg.toString()));
	
		sendMsg = new StringBuffer();
		sendMsg.append( "" );

		iSocket.send(TranHandle.strToArrayByte(sendMsg.toString()));

		logger.warn("send1 - 发送第一个包 end");

		logger.warn("send1 - 接收第一个包 Begin");
		
		ReceiveData rData = iSocket.receive();
	
		logger.warn("send1 - 接收第一个包 end");
	}

}