package org.rslai.socket.testtool.test.CXTV;

import org.apache.log4j.*;
import java.util.Iterator;
import java.util.LinkedList;
import org.rslai.socket.testtool.isocket.*;
import org.rslai.socket.testtool.ithread.*;
import org.rslai.socket.testtool.config.*;

public class TestTool extends MainThread {
	private ISocket localSocket;

	static Logger logger = Logger.getLogger(TestTool.class);

	public TestTool(User user) {
		super(user);
	}

	public void run() {
		try {
			logger.warn(user.getThreadName() + " START. userID=" + user.getUserInfoToString("ID"));

			yield();

			/*
			 * 创建连接
			 */
			logger.warn("创建连接 Begin");

			ISocketFactory iSocketFactory = new ISocketFactory(Data.getKeyValueToString("Financier5server"), Data.getKeyValueToInt("Financier5port"), ISocket.UTF8);
			ISocket SIPsocket = iSocketFactory.getISocket("org.rslai.socket.testtool.isocket.ISocketFinancier5");
			localSocket = SIPsocket;

			logger.warn("创建连接 End");

			/*
			 * 登录
			 */
			logger.warn("收发 Begin");

			send1 login = new send1();
			login.execute(localSocket, user);

			logger.warn("收发 End");
			yield();
				
			localSocket.close(); // 关闭所有连接
		}

		catch(Exception e) {
			e.printStackTrace();
			logger.error("Exception: " + e);
		}
		
	}

	/**
	 * 结束主线程和其所有子线程
	 */
	public void exit() {
	}

	public static void main(String args[]) {
		BasicConfigurator.configure();

		// 装载服务器配置及用户数据
		new LoadConfig("./datas/Config.xml");
		
		Iterator<String> it = Data.getTable("User").keySet().iterator();
		LinkedList<Thread> vUsers = new LinkedList<Thread>();
		while (it.hasNext()) {
			User user = new User(Data.getTableRow("User", it.next()));
			MainThread vUser = new TestTool( user );
			vUsers.addLast(vUser);
			vUser.start();

			new RunningTime(vUser, Data.getKeyValueToInt("RunningTime")).start(); // 启动运行时间线程
		}
	}
}

