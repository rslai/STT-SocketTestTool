package org.rslai.socket.testtool.md5;

import java.security.MessageDigest;

/**
 * 标准 MD5 编码类
 * @author rslai
 */
public class MD5Util {
	private final static String[] hexDigits = {
		"0", "1", "2", "3", "4", "5", "6", "7", 
		"8", "9", "a", "b", "c", "d", "e", "f"
	};

	public MD5Util() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 转换字节数组为16进制字串
	 * @param b 字节数组
	 * @return 16进制字串
	 */ 
	private String byteArrayToHexString(byte[] b) { 
		StringBuffer resultSb = new StringBuffer(); 
		for (int i = 0; i < b.length; i++) { 
			resultSb.append(byteToHexString(b[i])); 
		} 
		return resultSb.toString(); 
	} 

	private String byteToHexString(byte b) { 
		int n = b; 
		if (n < 0) {
			n = 256 + n;
		}
		int d1 = n / 16; 
		int d2 = n % 16; 
		return hexDigits[d1] + hexDigits[d2]; 
	} 

	/**
	 * MD5 编码
	 * @param origin 需要编码的字符串
	 * @return MD5 编码后的结果
	 */
	public String md5Encode(String origin) { 
		String resultString = null; 

		try { 
			resultString=new String(origin); 
			MessageDigest md = MessageDigest.getInstance("MD5"); 
			resultString=byteArrayToHexString(md.digest(resultString.getBytes())); 
		} catch (Exception ex) { 

		}
		return resultString; 
	} 

	public static void main(String[] args){
		/*
		 	在RFC 1321中，给出了Test suite用来检验你的实现是否正确：

			MD5 ("") = d41d8cd98f00b204e9800998ecf8427e
			MD5 ("a") = 0cc175b9c0f1b6a831c399e269772661
			MD5 ("abc") = 900150983cd24fb0d6963f7d28e17f72
			MD5 ("message digest") = f96b697d7cb7938d525a2f31aaf161d0
			MD5 ("abcdefghijklmnopqrstuvwxyz") = c3fcd3d76192e4007dfb496cca67e13b
		*/
		MD5Util md5Util = new MD5Util();
		
		System.out.println("Encode \"a\" : 0cc175b9c0f1b6a831c399e269772661");
		System.out.println("result     : " + md5Util.md5Encode("a"));
	}

}