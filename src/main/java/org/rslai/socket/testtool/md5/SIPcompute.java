package org.rslai.socket.testtool.md5;

/**
 * 根据　SIP　服务器给出挑战信息编码
 * @author rslai
 *
 */
public class SIPcompute {

	/**
	 * 根据　SIP　服务器给出挑战信息编码
	 * @param userID 用户 ID ，域名前的内容
	 * @param domainID 域名
	 * @param password 用户密码
	 * @param challenge 服务器传回的挑战信息
	 * @return 根据挑战字符串编码后的结果
	 */
	  public String compute(String userID, String domainID, String password, String challenge){
		String digestUri = "sip:myce.net.cn";
		String method = "REGISTER";

		/*
		 * 挑战信息通过三次 MD5 编码生成
		 *
		 * 1 ：userID:domainID:password    例如：lairongsheng:myce.net.cn:123456
		 * 2 ：method:digestUri            例如：REGISTER:sip:myce.net.cn;transport=tcp
		 * 3 ：HA1:challenge:HA2            例如：lairongsheng:myce.net.cn:123456
		 */
		MD5Util md5Util = new MD5Util();
		String HA1 = md5Util.md5Encode(userID + ":" + domainID + ":" + password);
		String HA2 = md5Util.md5Encode(method + ":" + digestUri);
		String HA3 = md5Util.md5Encode(HA1 +":" + challenge + ":" + HA2);

		return HA3;
	}

}
